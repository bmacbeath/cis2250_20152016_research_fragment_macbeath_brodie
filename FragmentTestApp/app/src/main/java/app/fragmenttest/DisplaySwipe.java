package app.fragmenttest;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

//import app.fragmenttest.fragments.FragmentAlex;
import app.fragmenttest.fragments.FragmentAlex;
import app.fragmenttest.fragments.FragmentAshley;
import app.fragmenttest.fragments.FragmentBrodie;
import app.fragmenttest.fragments.FragmentChelsea;
import app.fragmenttest.fragments.FragmentChrisM;
import app.fragmenttest.fragments.FragmentChrisS;
import app.fragmenttest.fragments.FragmentEvan;
import app.fragmenttest.fragments.FragmentJason;
import app.fragmenttest.fragments.FragmentJeffG;
import app.fragmenttest.fragments.FragmentJeffG.OnFragmentJeffGInteractionListener;
import app.fragmenttest.fragments.FragmentJeffN;
import app.fragmenttest.fragments.FragmentKyle;
import app.fragmenttest.fragments.FragmentMatthew;
import app.fragmenttest.fragments.FragmentNimna;
import app.fragmenttest.fragments.FragmentNotImplemented;
import app.fragmenttest.fragments.FragmentStephane;
import app.fragmenttest.fragments.FragmentSteven;
import app.fragmenttest.fragments.FragmentTristan;

public class DisplaySwipe extends AppCompatActivity implements FragmentKyle.OnFragmentKyleInteractionListener,
        OnFragmentJeffGInteractionListener,
        FragmentStephane.OnFragmentStephaneInteractionListener,
        FragmentAshley.OnFragmentAshleyInteractionListener,
        FragmentChelsea.OnFragmentChelseaInteractionListener,
        FragmentBrodie.OnFragmentBrodieInteractionListener,
        FragmentNimna.OnFragmentInteractionListener,
        FragmentMatthew.OnFragmentInteractionListener,
        FragmentEvan.OnFragmentEvanInteractionListener,
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {

    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    private int currentNumber = 0;
    private static final int NUMBER_OF_FRAGMENTS = 24;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        mDetector = new GestureDetectorCompat(this, this);
        mDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //default return value
        boolean itemSelected = super.onOptionsItemSelected(item);

        //create Toast for errors
        Toast newToast;

        //get menu item and perform action required
        switch (item.getItemId()) {
            case R.id.restart:
                itemSelected = true;
                finish();
                startActivity(new Intent(this, MainActivity.class));
                newToast = Toast.makeText(getApplicationContext(), "Restarting App", Toast.LENGTH_SHORT); //set Toast Message
                newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set Toast location
                newToast.show(); //pop Toast
                break;
            default:
                Log.d("ERROR", "Issue with menu options");
                break;
        }

        //return value
        return itemSelected;
    }

    //default gesture methods
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());

        //check if a fling happened
        if (event1.getAction() == MotionEvent.ACTION_DOWN && event2.getAction() == MotionEvent.ACTION_UP) {

            //create a fragment transaction
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            //check if fragment active
            Fragment findFragment = getFragmentManager().findFragmentByTag("fragment");
            if (findFragment != null) {
                ft.remove(findFragment);
            }

            //clear view
            findViewById(R.id.textViewSwipe).setVisibility(View.GONE);

            //swiping right to left
            if (event1.getX() - event2.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                ft = fm.beginTransaction();
                currentNumber++;
                Log.d("debug", "current number=" + currentNumber);
            } //swiping left to right
            else if (event2.getX() - event1.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {


                ft = fm.beginTransaction();
                currentNumber--;
                if (currentNumber < 0) {
                    currentNumber = NUMBER_OF_FRAGMENTS - 1;
                }
                Log.d("debug", "current number=" + currentNumber);
            }

            currentNumber = currentNumber % NUMBER_OF_FRAGMENTS;

            //change display according to index
            switch (currentNumber) {
                case 0:
                    Log.d("TEST", "Displaying BlankFragment.");
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 1:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_swipe, BlankFragment.newInstance(), "fragment").commit();
                    break;
                case 2: //Ben
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 3: //Evan
                    Log.d("2250", "No bundle");
                    FragmentEvan fe = new FragmentEvan();
                    Bundle data = new Bundle();
                    data.putString("preset", "no preset");
                    fe.setArguments(data);
                    ft.replace(R.id.layout_swipe, fe, "fragment").commit();
                    break;
                case 4: //Tristan
                    ft.replace(R.id.layout_swipe, new FragmentTristan(), "fragment").commit();
                    break;
                case 5: //AlexD
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 6:  //Kody
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 7: //Nimna
                    ft.replace(R.id.layout_swipe, new FragmentNimna(), "fragment").commit();
                    break;
                case 8:  //Chelsea
                    ft.replace(R.id.layout_swipe, new FragmentChelsea(), "fragment").commit();
                    break;
                case 9: //Jeff G
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_swipe, new FragmentJeffG(), "fragment").commit();
                    break;
                case 10:  //Gwen
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 11: //Matthew
                    ft.replace(R.id.layout_swipe, new FragmentMatthew(), "fragment").commit();
                    break;
                case 12: //Stephane
                    ft.replace(R.id.layout_swipe, new FragmentStephane(), "fragment").commit();
                    break;
                case 13: //Brodie
                    ft.replace(R.id.layout_swipe, new FragmentBrodie(), "fragment").commit();
                    break;
                case 14: //Chris S
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_swipe, new FragmentChrisM(), "fragment").commit();
                    break;
                case 15: //Steven
                    ft.replace(R.id.layout_swipe, new FragmentSteven(), "fragment").commit();
                    break;
                case 16: //Brandon
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 17: //Jason
                    ft.replace(R.id.layout_swipe, new FragmentJason(), "fragment").commit();
                    break;
                case 18: //JeffN
                    ft.replace(R.id.layout_swipe, new FragmentJeffN(), "fragment").commit();
                    break;
                case 19: //Kyle
                    Log.d("TEST", "Kyles");
                    ft.replace(R.id.layout_swipe, new FragmentKyle(), "fragment").commit();
                    break;
                case 20: //Ashley
                    ft.replace(R.id.layout_swipe, new FragmentAshley(), "fragment").commit();
                    break;
                case 21: //AlexP
                    ft.replace(R.id.layout_swipe, new FragmentAlex(), "fragment").commit();
                    break;
                case 22: //Marco
                    ft.replace(R.id.layout_swipe, new BlankFragment(), "fragment").commit();
                    break;
                case 23: //ChrisS
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_swipe, new FragmentChrisS(), "fragment").commit();
                    break;
                default:
                    Log.d("ERROR", "Current Index not found.");
                    ft.replace(R.id.layout_swipe, new FragmentNotImplemented(), "fragment").commit();
                    break;
            }
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + e1.toString() + e2.toString());
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        return true;
    }

    @Override
    public void onFragmentJeffGInteraction(String message) {
        Log.d("2250", "in onFragmentJeffGInteraction  activity received from fragment" + message);
        Toast.makeText(this, "interacted with Jeff fragment" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFragmentKyleInteraction(String message) {
        Toast.makeText(this, "Here is the date from Kyles fragment:" + message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Now going to start Evan's fragment:" + message, Toast.LENGTH_SHORT).show();
        //create a fragment transaction
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FragmentEvan fe = new FragmentEvan();
        try {
            Bundle data = new Bundle();
            data.putString("preset", message);
            fe.setArguments(data);
        } catch (Exception e) {

        }
        currentNumber = 3;
        ft.replace(R.id.layout_swipe, fe, "fragment").commit();
    }

    @Override
    public void onFragmentStephaneInteraction(String message) {

    }

    @Override
    public void onFragmentAshleyInteraction(String message) {

    }

    @Override
    public void onFragmentChelseaInteraction(String message) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentEvanInteraction(String message) {
        Toast.makeText(this, "This is what was entered on Evans fragment:" + message, Toast.LENGTH_SHORT).show();
        currentNumber++;
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.layout_swipe, new FragmentTristan(), "fragment").commit();
    }

    @Override
    public void onFragmentBrodieInteraction(String message) {

    }
}
