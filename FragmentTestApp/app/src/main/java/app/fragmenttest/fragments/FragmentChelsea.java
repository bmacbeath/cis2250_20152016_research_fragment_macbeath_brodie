package app.fragmenttest.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.TimeZone;

import app.fragmenttest.R;

/**
 * Created by Chelsea on 2016-01-18.
 */
public class FragmentChelsea extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentChelseaInteractionListener cListener;

    private Switch aSwitch;

    private EditText webChoice;
    private WebView webView;
    private Button webButton;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChelsea newInstance(String param1, String param2) {
        FragmentChelsea fragment = new FragmentChelsea();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentChelsea() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

//    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.fragment_fragment_chelsea, container, false);

        //switch
        aSwitch = (Switch) mainView.findViewById(R.id.switch1);
        aSwitch.setChecked(false);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FrameLayout frameLayout = (FrameLayout) mainView.findViewById(R.id.frameLayout);
                TextView text = (TextView) mainView.findViewById(R.id.textView);
                EditText editText = (EditText) mainView.findViewById(R.id.editText);
                ImageView image = (ImageView) mainView.findViewById(R.id.imageView);

                if(isChecked){ //day
                   frameLayout.setBackgroundColor(Color.parseColor("#0066ff"));
                    image.setImageResource(R.drawable.summer48);
                } else if (!isChecked){ //night
                   frameLayout.setBackgroundColor(Color.parseColor("#000823"));
                    image.setImageResource(R.drawable.brightmoon48);

                }
            }
        });


        //web site entered
        webChoice = (EditText) mainView.findViewById(R.id.editText);
        Log.d("CHELSEATEST", "Web site entered: " + webChoice.getText().toString());

        //button to go to web page
        webButton = (Button) mainView.findViewById(R.id.button);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String websiteEntered = webChoice.getText().toString();

                if(!websiteEntered.isEmpty()){
                    Log.d("CHELSEATEST", "Web site entered: " + websiteEntered);
                    try {

                        webView = (WebView) mainView.findViewById(R.id.webView);
                        webView.loadUrl("http://" + websiteEntered);
                    }catch(Exception e){
                        String errorException = "No website was entered!";
                        Toast.makeText(getActivity(), errorException, Toast.LENGTH_SHORT).show();
                        Log.d("CHELSEATEST", "Error in loading url: "+websiteEntered);
                    }
                } else {
                    String errorNoInput = "No website was entered!";
                    Toast.makeText(getActivity(), errorNoInput, Toast.LENGTH_SHORT).show();
                    Log.d("CHELSEATEST", "No website entered");
                }


            }
        });


        return mainView;
    }

    /**
     *
     */
    public interface OnFragmentChelseaInteractionListener {
        public void onFragmentChelseaInteraction(String message);
    }

}
