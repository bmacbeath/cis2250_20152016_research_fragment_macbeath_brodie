package app.fragmenttest.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentNimna#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentNimna extends Fragment  {
    private EditText tfNum1, tfNum2;
    private Button bttnAdd;
    private TextView tvAns;
    private Spinner spnrOperation;
    private String num1, num2;
    private String[] operation = {"+", "-", "/", "*"};
    double numAns;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener ;

    public FragmentNimna() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentNimna.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentNimna newInstance(String param1, String param2) {
        FragmentNimna fragment = new FragmentNimna();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_nimna, container, false);
        bttnAdd = (Button) view.findViewById(R.id.bttnCalc);
        tfNum1 = (EditText)view.findViewById(R.id.tfNum1);
        tfNum2 = (EditText)view.findViewById(R.id.tfNum2);
        tvAns = (TextView)view.findViewById(R.id.tvAns);
        spnrOperation = (Spinner)view.findViewById(R.id.spnrOperation);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item, operation);
        spnrOperation.setAdapter(adapter);


        bttnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = tfNum1.getText().toString();
                num2 = tfNum2.getText().toString();
                spnrOperation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (spnrOperation.getSelectedItemPosition() == 0) {
                            numAns = Double.parseDouble(num1) + Double.parseDouble(num2);
                        } else if (spnrOperation.getSelectedItemPosition() == 1) {
                            numAns = Double.parseDouble(num1) - Double.parseDouble(num2);
                        } else if (spnrOperation.getSelectedItemPosition() == 2) {
                            numAns = Double.parseDouble(num1) / Double.parseDouble(num2);
                        } else if (spnrOperation.getSelectedItemPosition() == 3) {
                            numAns = Double.parseDouble(num1) * Double.parseDouble(num2);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                tvAns.setText("Answer: " + numAns);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //public

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

    }

}
