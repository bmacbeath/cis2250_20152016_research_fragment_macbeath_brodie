package app.fragmenttest.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAshley.OnFragmentAshleyInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAshley#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAshley extends Fragment implements RatingBar.OnRatingBarChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static TextView textView1;
    private static TextView textView2;
    private static RatingBar ratingBar1;
    private static RatingBar ratingBar2;
    private View view;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String ratingSummary1 = "";
    private String ratingSummary2 = "";

    private OnFragmentAshleyInteractionListener mListener;

    public FragmentAshley() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAshley.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAshley newInstance(String param1, String param2) {
        FragmentAshley fragment = new FragmentAshley();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ashley, container, false);

        textView1 = (TextView) view.findViewById(R.id.textView1);
        textView2 = (TextView) view.findViewById(R.id.textView2);

        ratingBar1 = (RatingBar) view.findViewById(R.id.ratingBar);
        ratingBar2 = (RatingBar) view.findViewById(R.id.ratingBar2);


        ratingBar1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float stars, boolean isChanged){
                ratingSummary1 = updateRatingSummary(ratingBar, stars);
                Log.d("Rating", "Changing rating bar 1, rating is " + stars);
                textView1 = (TextView) view.findViewById(R.id.textView1);
                textView1.setText("You rated this product: "+ratingSummary1);
                Log.d("Summary", "Rating Summary: "+ratingSummary1);
            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float stars, boolean isChanged){
                updateRatingSummary(ratingBar, stars);
                Log.d("Rating2", "Changing rating bar 2, rating is " + stars);
                textView2 = (TextView) view.findViewById(R.id.textView2);
                textView2.setText("You rated this product: "+ratingSummary2);
                Log.d("Summary", "Rating Summary: " + ratingSummary2);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentAshleyInteraction(message);
        }
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentAshleyInteractionListener) {
//            mListener = (OnFragmentAshleyInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onRatingChanged(RatingBar ratingBar, float stars, boolean isChanged){
        updateRatingSummary(ratingBar, stars);

    }

    public String updateRatingSummary(RatingBar ratingBar, float stars){
        int numberStars = (int) stars;
        if(ratingBar==ratingBar1){
            switch (numberStars){
                case 0:
                    ratingSummary1 = "Unrated";
                    break;
                case 1:
                    ratingSummary1 = "Very Poor";
                    break;
                case 2:
                    ratingSummary1 = "Poor";
                    break;
                case 3:
                    ratingSummary1 = "Average";
                    break;
                case 4:
                    ratingSummary1 = "Good";
                    break;
                case 5:
                    ratingSummary1 = "Excellent";
                    break;

            }
            return ratingSummary1;
        } else {
            switch (numberStars){
                case 0:
                    ratingSummary2 = "Unrated";
                    break;
                case 1:
                    ratingSummary2 = "Very Poor";
                    break;
                case 2:
                    ratingSummary2 = "Poor";
                    break;
                case 3:
                    ratingSummary2 = "Average";
                    break;
                case 4:
                    ratingSummary2 = "Good";
                    break;
                case 5:
                    ratingSummary2 = "Excellent";
                    break;

            }
            return ratingSummary2;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentAshleyInteractionListener {
        // TODO: Update argument type and name
        void onFragmentAshleyInteraction(String message);
    }
}
