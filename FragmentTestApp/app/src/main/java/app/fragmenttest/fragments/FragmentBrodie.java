package app.fragmenttest.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextClock;

import java.util.TimeZone;

import app.fragmenttest.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentBrodieInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentBrodie#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBrodie extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String selectedTimeZoneString = "";

    TextClock clock;

    private OnFragmentBrodieInteractionListener mListener;

    public FragmentBrodie() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBrodie.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentBrodie newInstance(String param1, String param2) {
        FragmentBrodie fragment = new FragmentBrodie();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_brodie, container, false);

        Spinner selectedTimeZone = (Spinner) view.findViewById(R.id.spinnerTimeZone);

        clock = (TextClock) view.findViewById(R.id.textClock);

        selectedTimeZone.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    /**
                     * <p>Callback method to be invoked when an item in this view has been
                     * selected. This callback is invoked only when the newly selected
                     * position is different from the previously selected position or if
                     * there was no selected item.</p>
                     * <p>
                     * Impelmenters can call getItemAtPosition(position) if they need to access the
                     * data associated with the selected item.
                     *
                     * @param parent   The AdapterView where the selection happened
                     * @param view     The view within the AdapterView that was clicked
                     * @param position The position of the view in the adapter
                     * @param id       The row id of the item that is selected
                     */
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        switch (position) {
                            case 0:
                                selectedTimeZoneString = "America/Halifax";
                                Log.d("BRODIE TEST", "User selected Prince Edward Island for a timezone.");
                                break;
                            case 1:
                                selectedTimeZoneString = "Canada/Newfoundland";
                                Log.d("BRODIE TEST", "User selected Newfoundland and Labrador for a timezone.");
                                break;
                            case 2:
                                selectedTimeZoneString = "America/Toronto";
                                Log.d("BRODIE TEST", "User selected Ontario for a timezone.");
                                break;
                            case 3:
                                selectedTimeZoneString = "America/Winnipeg";
                                Log.d("BRODIE TEST", "User selected Manitoba for a timezone.");
                                break;
                            case 4:
                                selectedTimeZoneString = "America/Vancouver";
                                Log.d("BRODIE TEST", "User selected British Columbia for a timezone.");
                                break;
                            case 5:
                                selectedTimeZoneString = "America/Edmonton";
                                Log.d("BRODIE TEST", "User selected Alberta for a timezone.");
                                break;
                        }
                        clock.setTimeZone(selectedTimeZoneString);
                        Log.d("BRODIE TEST", "Time Zone Is: " + selectedTimeZoneString);

                    }

                    /**
                     * Callback method to be invoked when the selection disappears from this
                     * view. The selection can disappear for instance when touch is activated
                     * or when the adapter becomes empty.
                     *
                     * @param parent The AdapterView that now contains no selected item.
                     */
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );
            return view;
        }


        // TODO: Rename method, update argument and hook method into UI event
        public void onButtonPressed(String message) {
            if (mListener != null) {
                mListener.onFragmentBrodieInteraction(message);
            }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentBrodieInteractionListener) {
//            mListener = (OnFragmentBrodieInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentBrodieInteractionListener {
        // TODO: Update argument type and name
        void onFragmentBrodieInteraction(String message);
    }
}
