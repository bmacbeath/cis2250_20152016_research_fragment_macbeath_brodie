package app.fragmenttest.fragments;


import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentJason extends Fragment {

Button button;
    ImageView image1;
    ImageView image2;
    ImageView image3;
    public FragmentJason() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       final View view = inflater.inflate(R.layout.fragment_jason, container, false);
        image1 = (ImageView) view.findViewById(R.id.imageOne);
        image2 = (ImageView) view.findViewById(R.id.imageTwo);
        image3 =  (ImageView) view.findViewById(R.id.imageThree);
        image2.setVisibility(View.GONE);
        image3.setVisibility(View.GONE);
        image1.setVisibility(View.VISIBLE);
        button = (Button)view.findViewById(R.id.JasonFragmentButton);
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                if(image1.getVisibility() ==  View.VISIBLE){
                image1.setVisibility(View.GONE);

                image2.setVisibility(View.VISIBLE);
                image3.setVisibility(View.GONE);
            }else {
                    if (image2.getVisibility() == View.VISIBLE) {
                        image1.setVisibility(View.GONE);
                        image2.setVisibility(View.GONE);
                        image3.setVisibility(View.VISIBLE);
                    }else{
                        if(image3.getVisibility()==View.VISIBLE){
                            image1.setVisibility(View.VISIBLE);
                            image2.setVisibility(View.GONE);
                            image3.setVisibility(View.GONE);

                        }
                    }
                }
            }
        });
        return view;
    }

}
