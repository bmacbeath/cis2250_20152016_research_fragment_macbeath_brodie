package app.fragmenttest.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link FragmentNotImplemented#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentKyle extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String name = null;
    private Button button = null;
    TextView textView = null;
    DatePicker datePicker = null;
    private OnFragmentKyleInteractionListener mListener;

//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment Fragment1.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static FragmentNotImplemented newInstance(String param1, String param2) {
//        FragmentNotImplemented fragment = new FragmentNotImplemented();
//
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    public FragmentKyle() {
        // Required empty public constructor
    }

    public void setName(String name) {
        this.name = name;
        // textView.setText(name);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        textView = (TextView) getView().findViewById(R.id.textView1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("BJTEST", "onCreate - Fragment1");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //inflate the fragment view
        View view = inflater.inflate(R.layout.fragment_fragment_kyle, container, false);

        //get the date picker widget
        datePicker = (DatePicker) view.findViewById(R.id.dpId);


        //datePicker.init(2016, 2, 1, DatePicker.OnDateChangedListener(new OnDate));
        datePicker.init(
                2016, 2, 2,
                new DatePicker.OnDateChangedListener() {

                    //http://android-er.blogspot.ca/2013/06/example-of-using-datepicker-and.html
                    @Override
                    public void onDateChanged(DatePicker view,
                                              int year, int monthOfYear, int dayOfMonth) {

                        Toast.makeText(getActivity(), "onDateChanged", Toast.LENGTH_SHORT).show();

                        Log.d("2250", "Year: " + year + "\n" +
                                "Month of Year: " + (monthOfYear+1) + "\n" +
                                "Day of Month: " + dayOfMonth);

                        mListener.onFragmentKyleInteraction(year + "-" + (monthOfYear+1) + "-" + dayOfMonth);

                    }
                });


        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentKyleInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentKyleInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentKyleInteraction(String message);


    }
}
