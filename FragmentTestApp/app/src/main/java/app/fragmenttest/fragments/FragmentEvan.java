package app.fragmenttest.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEvan#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEvan extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String stringEntered;
    private int numberOfEntries = 0;
    private OnFragmentEvanInteractionListener mListener;
    // variables
    private Button btnAddItem = null;
    private LinearLayout checklist = null;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentEvan newInstance(String param1, String param2) {
        FragmentEvan fragment = new FragmentEvan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentEvan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        stringEntered = "";

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_evan, container, false);

        // set up button from the layout
        btnAddItem = (Button) view.findViewById(R.id.btnAddItem);
        checklist = (LinearLayout) view.findViewById(R.id.checklist);

        // get arguments
        Bundle args = getArguments();
        String preset = args.getString("preset");
        CheckBox cb = new CheckBox(getActivity());
        cb.setText(preset);
        checklist.addView(cb);

        // set up an AlertDialog to get user input with
        final EditText input = new EditText(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Item")
                .setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button, add new Checkbox item
                CheckBox cb = new CheckBox(getActivity());
                cb.setText(input.getText());
                //Add the text to a String
                stringEntered += input.getText();
                numberOfEntries++;

                //If two entries send notice to the activity to go to the next fragment
                if (numberOfEntries > 1) {
                    mListener.onFragmentEvanInteraction(stringEntered);
                }

                checklist.addView(cb);
                Log.d("ADD ITEM", "New item was added: " + input.getText());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog, do nothing
                Log.d("ADD ITEM", "Add item was cancelled.");
            }
        });
        final AlertDialog dialog = builder.create();

        // attach event listener to the button
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("ADD ITEM", "Add Item button was clicked.");
                dialog.show();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentEvanInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentEvanInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentEvanInteraction(String message);


    }

}
