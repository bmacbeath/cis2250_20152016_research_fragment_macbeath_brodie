package app.fragmenttest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createAlertDialog();
    }

    public void createAlertDialog() {

        //create an alert dialog for user to choose type of activity
        new AlertDialog.Builder(this)
                .setTitle("Please Choose Activity")
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_swipe, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getBaseContext(), DisplaySwipe.class));
                    }
                })
                .setNegativeButton(R.string.dialog_list, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getBaseContext(), DisplayList.class));
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //default return value
        boolean itemSelected = super.onOptionsItemSelected(item);

        //create Toast for errors
        Toast newToast;

        //get menu item and perform action required
        switch (item.getItemId()) {
            case R.id.restart:
                itemSelected = true;
                finish();
                startActivity(new Intent(this, MainActivity.class));
                newToast = Toast.makeText(getApplicationContext(), "Restarting App", Toast.LENGTH_SHORT); //set Toast Message
                newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set Toast location
                newToast.show(); //pop Toast
                break;
            default:
                Log.d("ERROR", "Issue with menu options");
                break;
        }

        //return value
        return itemSelected;
    }
}
