package app.fragmenttest.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import app.fragmenttest.BlankFragment;
import app.fragmenttest.MainActivity;
import app.fragmenttest.R;


/**
 * SEEKBAR:
 *  http://examples.javacodegeeks.com/android/core/widget/seekbar/android-seekbar-example/
 * PROGRESS BAR:
 *  http://www.java2s.com/Code/Android/UI/UsingThreadandProgressbar.htm
 *
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class FragmentChrisS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final String TAG = MainActivity.class.getSimpleName();
    // Objects declared for SeekBar
    private SeekBar seekBar;
    private TextView textView, textViewSeekBarStatus;
    private String textString;
    // Objects declared for ProgressBar
    private ProgressBar progressBar;
    private int progressStatus = 0;
    private int progress = 0;
    private Handler handler = new Handler();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentChrisS() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

//    private int doSomeWork() {
//        try {
//            // ---simulate doing some work---
//            Thread.sleep(200);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        progress += 5;
//        return progress;
//    }

    private void initializeVariables(View view) {
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        textView = (TextView) view.findViewById(R.id.textView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        textViewSeekBarStatus = (TextView) view.findViewById(R.id.textViewSeekBarStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chris_s, container, false);
        initializeVariables(view);
        // Disable the SeekBar
        seekBar.setEnabled(false);

        /*

            PROGRESS BAR CODE

         */

        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus = doSomeWork();
                    handler.post(new Runnable() {
                        // LOADER STARTS AND PROGRESSES HERE
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            textView.setText("Loading. Please wait..." + progressStatus + "/" + progressBar.getMax());
                        }
                    });
                }

                handler.post(new Runnable() {
                    // LOADER COMPLETES HERE
                    public void run() {
                        // ---0 - VISIBLE; 4 - INVISIBLE; 8 - GONE---
                        textView.setText("You can now start seeking");
                        progressBar.setVisibility(View.GONE);
                        seekBar.setEnabled(true);

                    }
                });
            }

            private int doSomeWork() {
                try {
                    // ---simulate doing some work---
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                progress += 5;
                return progress;
            }
        }).start();

//        while(progressStatus < 100) {
//            textView.setText("Loading. Please wait. (" + progressStatus + "%)");
//            progressStatus = doSomeWork();
//            progressBar.setProgress(progressStatus);
//        }
//        progressBar.setVisibility(View.GONE);
//        seekBar.setEnabled(true);

        // Start long running operation in a background thread
//        new Thread(new Runnable() {
//            public void run() {
//                while (progressStatus < 100) {
//                    progressStatus += 5;
//                    // Update the progress bar and display the
//                    //current value in the text view
//                    handler.post(new Runnable() {
//                        public void run() {
//                            progressBar.setProgress(progressStatus);
//                            textView.setText("Loading. Please wait..." + progressStatus+"/"+progressBar.getMax());
//                        }
//                    });
//                    try {
//                        // Sleep for 200 milliseconds.
//                        //Just to display the progress slowly
//                        Thread.sleep(200);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//
////                Log.d(TAG, "Progress Loaded Ended");
//
//            }
//        }).start();



        /*

            SEEK BAR CODE

         */


        // Initialize TextView
        textString = "Donating: $" + seekBar.getProgress() + "/$" + seekBar.getMax();
        textView.setText("Donating: $" + seekBar.getProgress() + "/$" + seekBar.getMax());

        // Listener for the SeekBar
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            // Initially set to start at 0
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
//                Toast.makeText(getActivity().getApplicationContext(), "Changing SeekBar...", Toast.LENGTH_SHORT).show();
                textViewSeekBarStatus.setText("You are changing the SeekBar");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(getActivity().getApplicationContext(), "Started SeekBar...", Toast.LENGTH_SHORT).show();
                textViewSeekBarStatus.setText("You are pressing the SeekBar");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
//                textView.setText(textString);
                textView.setText("Donating: $" + seekBar.getProgress() + "/$" + seekBar.getMax());
                // Shows a secondary bar representing what will be changed
                seekBar.setSecondaryProgress(seekBar.getProgress());
//                Toast.makeText(getActivity().getApplicationContext(), "Stopped SeekBar...", Toast.LENGTH_SHORT).show();
                textViewSeekBarStatus.setText("You have stopping moving the SeekBar.  Donate now?");
            }
        });

        return view;
    }

}
