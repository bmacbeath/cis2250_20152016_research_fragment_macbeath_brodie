package app.fragmenttest.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class FragmentJeffN extends Fragment {


    public FragmentJeffN() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jeff_n, container, false);
        Button btnSend = (Button) view.findViewById(R.id.btnSend);
        final EditText to = (EditText) view.findViewById(R.id.editTxtTo);
        final EditText message = (EditText) view.findViewById(R.id.editTxtMessage);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(" FragmentJeffN btnSend", "onClick: ");
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setType("vnd.android-dir/mms-sms");
                intent.setData(Uri.parse("sms:" + to.getText().toString()));
                intent.putExtra("address", to.getText().toString());
                intent.putExtra("sms_body", message.getText().toString());

                //Intent choose = Intent.createChooser(intent,"Send a Message");
                startActivity(intent);

            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

}
