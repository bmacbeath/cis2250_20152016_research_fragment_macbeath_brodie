package app.gradecalculator;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * @author Kody Duncan
 * @since 1/15/2016
 *
 * When called creates a ListFragment of Deliverables.
 * The view will dynamically change if the user changes orientation to allow ListFragment and InfoFragment on same page.
 */

public class DeliverableFragment extends ListFragment {

    private ArrayList<Deliverable> deliverables;
    private boolean panelActive;
    private int currentIndex = 0;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //make sure ArrayList has data
        if(getActivity().getIntent().getSerializableExtra("deliverables") != null) {

            //get deliverables ArrayList
            deliverables = (ArrayList<Deliverable>) getActivity().getIntent().getSerializableExtra("deliverables");

            //check if all grades entered
            createMarks();

            //create an array of deliverable names
            int index = deliverables.size();
            String[] deliverableNames = new String[index];
            for (int x = 0; x < index; x++) {
                deliverableNames[x] = deliverables.get(x).getName();
            }

            //create an ArrayAdapter to display the name as a list in fragment view
            ArrayAdapter<String> setDeliverableList = new ArrayAdapter<String>(
                    getActivity(),
                    android.R.layout.simple_list_item_activated_1,
                    deliverableNames);

            setListAdapter(setDeliverableList);

            //update currentIndex after state change
            if (savedInstanceState != null) {
                currentIndex = savedInstanceState.getInt("curIndex", 0);
            }

            //check if content panel active
            View contentFrame = getActivity().findViewById(R.id.contentFrame);
            panelActive = contentFrame != null && contentFrame.getVisibility() == View.VISIBLE;

            if(panelActive) {
                //allows user to select one list item at a time
                getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

                //display panel
                showDetails(currentIndex);
            }
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curIndex", currentIndex);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(position);
    }

    public void showDetails(int position) {

        //update current index
        currentIndex = position;

        //display panel based on phone orientation
        if(panelActive) {

            //highlight selected item
            getListView().setItemChecked(position, true);

            //object which represents current layout
            InfoFragment showDetails = (InfoFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

            //assign object data
            if(showDetails == null || showDetails.getCurrentIndex() != currentIndex) {

                //get details fragment
                showDetails = InfoFragment.newInstance(position, deliverables);

                //start Fragment transactions
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();

                //replace any current fragment new fragment
                fragmentTransaction.replace(R.id.contentFrame, showDetails);

                //fragment fades in or out
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }
        } else {
            //create new Activity and pass data
            Intent newActivity = new Intent(getActivity(), DisplayFragments.class);
            newActivity.putExtra("index", position);
            newActivity.putExtra("deliverables", deliverables);
            getActivity().finish();
            startActivity(newActivity);
        }
    }

    //updates list if all grades entered
    public void createMarks() {

        //check if already created
        int index = (deliverables.size()-1);
        if(!deliverables.get(index).getName().equalsIgnoreCase("Course Marks")) {

            double courseMark = 0; //get course mark
            boolean noGrades = false; //check if grades entered

            //calculate users grade
            for(Deliverable deliverable : deliverables) {
                if(deliverable.getGrade() == -1) {
                    noGrades = true;
                    break;
                } else {
                    courseMark += deliverable.getGrade();
                }
            }

            //set course marks
            if(!noGrades) {
                deliverables.add(new Deliverable("Course Marks", courseMark, 0));
                Intent newIntent = getActivity().getIntent();
                getActivity().getIntent().putExtra("deliverables", deliverables);
            }
        }
    }
}
