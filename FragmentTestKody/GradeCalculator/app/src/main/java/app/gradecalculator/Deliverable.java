package app.gradecalculator;

import java.io.Serializable;

/**
 * @author Kody Duncan
 * @since 1/15/2016
 *
 * This is an object class which can be passed too different Activities and Fragments using the Serializable method.
 */

public class Deliverable implements Serializable {

    private String name;
    private double weight;
    private double grade = -1;

    //allows user to create object without grade
    public Deliverable(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    //allows user to create object with grade
    public Deliverable(String name, double weight, double grade) {
        this.name = name;
        this.weight = weight;
        this.grade = grade;
    }

    //default getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}
