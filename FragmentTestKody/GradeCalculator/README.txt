PLEASE READ THE MAIN PROJECTS README ON HOW TO SETUP! (ALWAYS UPLOAD/UPDATE DATABASE BEFORE USING IF REQUIRED!)

Quick Overview:

	-Application allows users to enter deliverable information then input grades for deliverables.
		-Once grades are enters provides users an Course Mark. 
			-This is done using Intent to pass an Arraylist of deliveravle objects.
	
The Grade Calculator App has the following updates:

	-Linear, Frame, Fragment, and Relative Layouts
	-EditText, Text, and Scroll Views
	-Also Buttons and Toast (Toast have custom gravity settings)

Functionality Added:

	-Main Page
		-Allows users to add deliverables until weight reaches 100.
		-Weight must between 0 and 100.
	-Deliverable ListFragment (Appears after deliverables are added.)
		-Display an list of deliverable objects (which can be selected).
		-Automatically updates list with "Course Marks" when all grades added.
	-Deliverable Information Fragment (Appears after a ListFragment is selected.)
		-Displays selected deliverable information.
		-Allows grade input and updates deliverable once inputed.
	-Landscape Mode
		-Displays both fragments side by side.
		-Provides same functions as would in Portrait Mode.
		
Warnings/Issues:

	-None Found (But Possible)